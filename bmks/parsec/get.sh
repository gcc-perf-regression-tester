#!/bin/bash

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="parsec"
bmks="amd64-linux.gcc-pthreads/bin/blackscholes
amd64-linux.gcc-pthreads/bin/bodytrack
amd64-linux.gcc-pthreads/bin/facesim
amd64-linux.gcc-pthreads/bin/ferret
amd64-linux.gcc-pthreads/bin/swaptions
amd64-linux.gcc-pthreads/bin/fluidanimate
amd64-linux.gcc-pthreads/bin/x264
amd64-linux.gcc-pthreads/bin/canneal
amd64-linux.gcc-pthreads/bin/streamcluster
amd64-linux.gcc-openmp/bin/blackscholes
amd64-linux.gcc-openmp/bin/bodytrack
amd64-linux.gcc-openmp/bin/freqmine
amd64-linux.gcc-tbb/bin/blackscholes
amd64-linux.gcc-tbb/bin/bodytrack
amd64-linux.gcc-tbb/bin/swaptions
amd64-linux.gcc-tbb/bin/fluidanimate
amd64-linux.gcc-tbb/bin/streamcluster"

parse_nb () {
    nb=`grep -m 1 "$b run time:" $tag | awk '{ print $4 }'`
}

. $HOME/gcc/test/plot.sh
