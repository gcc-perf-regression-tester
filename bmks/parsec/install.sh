#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

wget http://coblitz.codeen.org/parsec.cs.princeton.edu/download/2.1/parsec-2.1-core.tar.gz
wget http://coblitz.codeen.org/parsec.cs.princeton.edu/download/2.1/parsec-2.1-native.tar.gz
tar zxf parsec-2.1-core.tar.gz
tar zxf parsec-2.1-native.tar.gz

patch -p0 < config.diff
patch -p0 < openssl.diff
patch -p0 < ferret.diff

