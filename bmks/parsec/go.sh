#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="parsec"
. $HOME/gcc/test/set_config.sh
p=$B/parsec-2.1

if ! test -e $p/env.sh; then
    echo "Parsec benchmarks are not installed, skipping."
    exit 1
fi

cd $p
export PARSECDIR=$p
source env.sh
parsecmgmt -a fullclean -p all
parsecmgmt -a fulluninstall -p all
rm -rf $p/log/
mkdir -p $p/log/

run_parsec_bmks () {
    for b in $2; do
	parsecmgmt -a build -p $b -c gcc-$1
	parsecmgmt -a run   -p $b -c gcc-$1 -i native -n $procs
	catlog `ls -1tr $p/log/amd64-linux.gcc-$2/run_* | tail -1` "sequenceB_261" "Processing frame" "Face_Data" "Frame" "corel" "queries" "SwaptionPrice"
	parsecmgmt -a fullclean -p all
	parsecmgmt -a fulluninstall -p all
    done
}

run_parsec_bmks pthreads 'blackscholes bodytrack facesim ferret swaptions fluidanimate x264 canneal streamcluster'
run_parsec_bmks openmp 'blackscholes bodytrack freqmine'
run_parsec_bmks tbb 'blackscholes bodytrack swaptions fluidanimate streamcluster'

. $X/finish_log.sh
