#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html


wget http://repo.or.cz/w/phoronix-static.git/snapshot/d5e1832575b99d9d31132484f86bdd322e3abe0c.tar.gz
tar zxf d5e1832575b99d9d31132484f86bdd322e3abe0c.tar.gz
mv phoronix-static/* .
./install-extra.sh
