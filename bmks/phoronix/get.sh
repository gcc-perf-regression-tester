#!/bin/bash

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="phoronix"
bmks="apache bullet byte cachebench clomp compress-7zip compress-lzma
compress-pbzip2 crafty c-ray dcraw encode-ape encode-flac encode-mp3
encode-ogg encode-wavpack espeak ffmpeg fhourstones gcrypt gmpbench
gnupg graphics-magick himeno hmmer john-the-ripper mafft mencoder
minion mrbayes nero2d nginx npb n-queens openssl pgbench phpbench
povray ramspeed sample-program scimark2 smallpt stream sudokut
systester tachyon x264"

parse_nb () {
    nb=`grep -m 1 "$b run time:" $tag | awk '{ print $4 }'`
}

. $HOME/gcc/test/plot.sh
