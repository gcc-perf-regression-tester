#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <sys/time.h>
#include <unistd.h>

#define TMAX 4000
#define NMAX 1000000

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#ifdef TIME
double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif



static double a[NMAX], b[NMAX];

void init(int N)
{
	int k;
	for (k=0; k<N; k++) {
		a[k] = 1.0 + ((double)k)/N;
	}
}

void print (int N)
{
	int k;
	for (k=0; k<N; k++) {
		fprintf(stdout, "%lf ", a[k]);
		if (k%80 == 20) fprintf(stdout, "\n");
	}
	fprintf(stdout, "\n");
}


void jac(long T, long N) 
{
	int t, i, j;

#pragma scop
	for (t = 0; t < T; t++) {
		for (i = 2; i < N - 1; i++) {
			b[i] = ((double)333/1000) * (a[i - 1] + a[i] + a[i + 1]); // S1
		}
		for (j = 2; j < N - 1; j++) {
			a[j] = b[j]; // S2
		}
	}
#pragma endscop
}


int main()
{
  double t_start, t_end;
	long T, N;
	T = TMAX; N=NMAX;

	init(N);

  IF_TIME(t_start = rtclock());

  jac(T,N);

  IF_TIME(t_end = rtclock());
  IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));

  if (fopen(".test", "r"))  {
    print(N);
  }

  return 0;
}
