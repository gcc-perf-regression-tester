#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <sys/time.h>

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#ifdef TIME
double rtclock()
{
	struct timezone Tzp;
	struct timeval Tp;
	int stat;
	stat = gettimeofday (&Tp, &Tzp);
	if (stat != 0) printf("Error return from gettimeofday: %d",stat);
	return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif


#define NMAX 1000
#define TMAX 500

static double a[NMAX][NMAX];

void seidel(long N, long T) {

    long i,j,t;

#pragma scop
    for (t=0; t<=T-1; t++)  {
        for (i=1; i<=N-2; i++)  {
            for (j=1; j<=N-2; j++)  {
                a[i][j] = (a[i-1][j-1] + a[i-1][j] + a[i-1][j+1] 
                        + a[i][j-1] + a[i][j] + a[i][j+1]
                        + a[i+1][j-1] + a[i+1][j] + a[i+1][j+1])/9.0;
            }
        }
    }
#pragma endscop
}

int main()
{
  int i, j;
  double t_start, t_end;

  long N=NMAX;

  for (i=0; i<N; i++)	{
    for (j=0; j<N; j++)	{
      a[i][j] = i + j/2.0;
    }
  }

  IF_TIME(t_start = rtclock());
  seidel(NMAX,TMAX);
  IF_TIME(t_end = rtclock());
  IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));

  if (fopen(".test", "r"))  {
    for (i=0; i<N; i++)	{
      for (j=0; j<N; j++)	{
        fprintf(stdout, "%lf ", a[i][j]);
      }
    }
    fprintf(stdout, "\n");
  }
  return 0;
}
