#include <stdio.h>
#include <math.h>

#include <sys/time.h>

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#ifdef TIME
double rtclock()
{
	struct timezone Tzp;
	struct timeval Tp;
	int stat;
	stat = gettimeofday (&Tp, &Tzp);
	if (stat != 0) printf("Error return from gettimeofday: %d",stat);
	return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif


#define SIZE 10000

static double P[SIZE][SIZE];

void sor(int N1, int N2){
  int i, j, k;

#pragma scop
  for(i=1; i<N1-1; i++) {
    for(j=1; j<N2-1; j++) {
      P[i][j] = (P[i][j] + P[i][j-1] + P[i][j+1] + P[i-1][j] + P[i+1][j]) / 5;
    }
  }
#pragma endscop
}


int main(int argc, char *argv)
{
  int i, j;
  int N1 = SIZE-1, N2 = SIZE-1;
  double t_start, t_end;

  for(i=0; i<SIZE; i++){
    for(j=0; j<SIZE; j++){
      P[i][j] = 0.30 + i*j*0.5;
    }
  }

  IF_TIME(t_start = rtclock());
  sor(N1, N2);
  IF_TIME(t_end = rtclock());
  IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));

  if (fopen(".test", "r"))  {
    for (i=0; i<SIZE/4; i++)	{
      for (j=0; j<SIZE/4; j++)	{
        fprintf(stdout, "%lf ", P[i][j]);
      }
    }
    fprintf(stdout, "\n");
  }

  return 0;
}
