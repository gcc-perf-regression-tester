#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define NMAX 2000

#define MEASURE_TIME

static double a[NMAX][NMAX], b[NMAX][NMAX], c[NMAX][NMAX];

void ssyr2k(long N) {
	int i,j,k;
	
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=j; k<N; k++) {
				c[j][k] += a[i][j] * b[i][k] + b[i][j] * a[i][k];
			}
		}
	}
}


int main()
{
	struct timeval start;
	struct timeval end;
    long N=NMAX;
	int i,j;
	int errors = 0;


	gettimeofday(&start, NULL);
    ssyr2k(N);
	gettimeofday(&end, NULL);


#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
    return (errors != 0);
}
