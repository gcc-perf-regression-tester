#include <stdio.h>
#include <math.h>
#include <sys/time.h>


//#define SELFTEST 1
#define MEASURE_TIME 1

#define NMAX 8000

#define alpha 1
#define beta 1

double A[NMAX][NMAX];
double x[NMAX];
double u1[NMAX];
double u2[NMAX];
double v2[NMAX];
double v1[NMAX];
double w[NMAX];
double y[NMAX];
double z[NMAX];

double AA[NMAX][NMAX];
double xx[NMAX];
double ww[NMAX];


void gemver(long N) {
	int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            A[i][j] = A[i][j] + u1[i]*v1[j] + u2[i]*v2[j];
         }
     }

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            x[i] = x[i] + A[j][i]*y[j];
        }
     }


    for (i=0; i<N; i++) {
        x[i] = x[i] + z[i];
    }

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            w[i] = w[i] +  A[i][j]*x[j];
        }
    }

}


#if SELFTEST
void ref(long N) {
	int i, j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            AA[i][j] = AA[i][j] + u1[i]*v1[j] + u2[i]*v2[j];
         }
     }

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            xx[i] = xx[i] + AA[j][i]*y[j];
        }
     }


    for (i=0; i<N; i++) {
        xx[i] = xx[i] + z[i];
    }

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            ww[i] = w[i] +  AA[i][j]*xx[j];
        }
    }

}
#endif

int main()
{
	struct timeval start;
	struct timeval end;
	int i, j, k;
	long N=NMAX;
	int errors = 0;

#if SELFTEST
    for (i=0; i<N; i++) {
        u1[i] = i;
        u2[i] = (i+1)/N/2.0;
        v1[i] = (i+1)/N/4.0;
        v2[i] = (i+1)/N/6.0;
        y[i] = (i+1)/N/8.0;
        z[i] = (i+1)/N/9.0;
        x[i] = xx[i] = 0.0;
        w[i] = ww[i] = 0.0;
        for (j=0; j<N; j++) {
            A[i][j] = AA[i][j] = ((double) i*j)/N;
        }
    }
#endif

	gettimeofday(&start, NULL);
	gemver(N);
	gettimeofday(&end, NULL);

#if SELFTEST
	printf("SELFTEST\n");
	ref(N);

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (A[i][j] != AA[i][j]) {
				errors++;
				printf("expeAted AA[%d][%d] = %Lf found A[%d][%d] = %Lf\t=>\t10^15*Diff is: %Lf\n", 
							i, j, AA[i][j], 
							i, j, A[i][j], (AA[i][j] - A[i][j]) * 1000000000000000.0);
			}
		}
	}

	if(errors != 0) printf("%d errors, FAILURE\n", errors);
	else printf("SUCCESS\n");  
#endif

#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif

  return errors != 0;
}
