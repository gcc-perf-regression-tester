#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#ifdef TIME
double rtclock()
{
	struct timezone Tzp;
	struct timeval Tp;
	int stat;
	stat = gettimeofday (&Tp, &Tzp);
	if (stat != 0) printf("Error return from gettimeofday: %d",stat);
	return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif

#define NMAX 1500
#define TMAX 1500

static double a[NMAX][NMAX], b[NMAX][NMAX];

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void jac2d(long T, long N) {

	int t, i, j;

#pragma scop
	for (t = 0; t < T; t++) {
		for (i = 1; i < N - 1; i++) {
			for (j = 1; j < N-1; j++) {
				b[i][j] = (
						a[i-1][j-1] + a[i-1][j] + a[i-1][j+1] +
						a[i][j-1]   + a[i][j]   + a[i][j+1] + 
						a[i+1][j-1] + a[i+1][j] + a[i+1][j+1]
						) / 9.0; // S1
			}
		}
		for (i = 1; i < N - 1; i++) {
			for (j = 1; j < N-1; j++) {
				a[i][j] = b[i][j]; // S2
			}
		}
	}
#pragma endscop
} // end jac()

int main()
{
	double t_start, t_end;
	long T, N;
	int i,j;
	T = TMAX; N=NMAX;

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			a[i][j] = i*j*0.5;
			b[i][j] = 0.0;
		}
	}

	// Compute Jacobi relaxation.
	IF_TIME(t_start = rtclock());
	jac2d(T,N);
	IF_TIME(t_end = rtclock());
	IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));

	// Compare results
	if (fopen(".test", "r"))	{
		for (i = 1; i < N - 1; i++) {
			for (j = 1; j < N - 1; j++) {
				printf("%lf ", b[i][j]);
			}
		}
	}

	return 0;
}
