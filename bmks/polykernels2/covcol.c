#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/time.h>
#include <math.h>

#define my_sqrt_array(x,j) sqrt(x[j])

#define N 1000
#define M 1000

float float_n = (float) N;
float eps = 0.005;
float stddev[M + 1];
float data[M + 1][N + 1];
float mean[M + 1];
float symmat[M + 1][M + 1];

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

void init_array()
{
    int i, j;

    for (i=0; i<M; i++) {
      for (j=0; j<N; j++) {
        data[i][j] = ((float) i*j)/N;
      }
    }
}


void print_array()
{
  int i, j;
  for (i=0; i<M; i++) {
    for (j=0; j<M; j++) {
      fprintf(stdout, "%0.2lf ", symmat[i][j]);
      if (i%80 == 20) fprintf(stdout, "\n");
    }
  }
  fprintf(stdout, "\n");
}

#ifdef TIME
double rtclock()
{
  struct timezone Tzp;
  struct timeval Tp;
  int stat;
  stat = gettimeofday (&Tp, &Tzp);
  if (stat != 0) printf("Error return from gettimeofday: %d",stat);
  return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif

#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif


int main(int argc, char** argv)
{
  double t_start, t_end;
  int i, j, j1, j2;
  int n = N;
  int m = M;

  init_array();

  IF_TIME(t_start = rtclock());

#pragma scop
  /* Determine mean of column vectors of input data matrix */
  for (j = 1; j <= m; j++)
  {
    mean[j] = 0.0;
    for (i = 1; i <= n; i++)
      mean[j] += data[i][j];
    mean[j] /= float_n;
  }

  /* Center the column vectors. */
  for (i = 1; i <= n; i++)
    for (j = 1; j <= m; j++)
      data[i][j] -= mean[j];

  /* Calculate the m * m covariance matrix. */
  for (j1 = 1; j1 <= m; j1++)
    for (j2 = j1; j2 <= m; j2++)
    {
      symmat[j1][j2] = 0.0;
      for (i = 1; i <= n; i++)
        symmat[j1][j2] += data[i][j1] * data[i][j2];
      symmat[j2][j1] = symmat[j1][j2];
    }
#pragma endscop

  IF_TIME(t_end = rtclock());
  IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));


  if (fopen(".test", "r"))  {
    print_array();
  }

  return 0;
}
