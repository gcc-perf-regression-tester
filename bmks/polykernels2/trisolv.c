#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#ifdef TIME
double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif

#define NMAX 2000

static double B[NMAX][NMAX], L[NMAX][NMAX];

void trisolv(long N) {

  long i,j,k;

#pragma scop
  for (i=0;i<=N-1;i++) {
    for (j=0;j<=N-1;j++) {
      for (k=0;k<=j-1;k++) {
        B[j][i]=B[j][i]-L[j][k]*B[k][i];
      }
      B[j][i]=B[j][i]/L[j][j];
    }
  } 
#pragma endscop
} // end trisolv()


int main()
{
  double t_start, t_end;

  long N=NMAX;
  int i,j;

  for (i = 0; i < NMAX; i++) {
    for (j = 0; j < NMAX; j++) {
      B[i][j]  = 0.45+i+j;
      L[i][j] = 3.45 + (i+j)*((double)NMAX);
    }
  }

  IF_TIME(t_start = rtclock());

  trisolv(N);

  IF_TIME(t_end = rtclock());
  IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));

  if (fopen(".test", "r")) {
    for (i = 0; i < NMAX; i++) {
      for (j = 0; j < NMAX; j++) {
        printf("%lf ", B[i][j]);
        if (j%80==0)  printf("\n");
      }
    }
  }

  return 0;
}
