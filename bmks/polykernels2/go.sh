#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="polykernels2"
. $HOME/gcc/test/set_config.sh

bmks="corcol.c covcol.c doitgen.c dsyr2k.c dsyrk.c gemver.c jac2d.c
jac.c lud.c mmm.c seidel.c sor.c ssymm.c ssyr2k.c strmm.c strsm.c
tmm.c trisolv.c"

for b in $bmks; do
    out="$B/out.txt"
    > $out
    gcc $FLAGS $B/$b -o $B/$b.exe
    $B/$b.exe > $out
    rm $B/$b.exe
    t=`grep "Elapsed Time" $out | cut -d '=' -f 2`
    echo "$b $t" >> $log
done

. $X/finish_log.sh
