#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#define NMAX 2000

#ifdef TIME
double rtclock()
{
	struct timezone Tzp;
	struct timeval Tp;
	int stat;
	stat = gettimeofday (&Tp, &Tzp);
	if (stat != 0) printf("Error return from gettimeofday: %d",stat);
	return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif

double A[NMAX][NMAX];
double B[NMAX][NMAX];
double C[NMAX][NMAX];

void mmm(long Ni, long Nj, long Nk) {
	int i, j, k;

#pragma scop
	for(i = 0; i < Ni; i++) {
		for(j=0; j < Nj; j++) {
			for(k=0; k < Nk; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
#pragma endscop
}


int main()
{
	long Ni=NMAX;
	long Nj=NMAX;
	long Nk=NMAX;
	double t_start, t_end;
	int i, j, k;

	for(i = 0; i < Ni; i++) {
		for(j=0; j < Nj; j++) {
			A[i][j] = i+j;
			B[i][j] = i*j/((double)2.0);
			C[i][j] = 0.0;
		}
	}

	IF_TIME(t_start = rtclock());

	mmm(Ni, Nj, Nj);

	IF_TIME(t_end = rtclock());
	IF_TIME(printf("Elapsed Time=%0.6lf\n", t_end - t_start));

	if (fopen(".test", "r")) {
		for (i=0; i<Ni; i++)    {
			for (j=0; j<Nj; j++)    {
				printf("%lf ", C[i][j]);
				if (j%80 == 0) {
          printf("\n");
        }
			}
		}
	}

	return 0;
}
