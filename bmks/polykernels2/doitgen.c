#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <sys/time.h>

#define NMAX 31

double A[NMAX][NMAX][NMAX];
double sum[NMAX][NMAX][NMAX];
double C4[NMAX][NMAX];

#define TIME
#ifdef TIME
#define IF_TIME(foo) foo;
#else
#define IF_TIME(foo)
#endif

#ifdef TIME
double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}
#endif



void doitgen(long N) {
	int r, q, p, s;

#pragma scop
    for(r = 0; r< N; r++)  {
        for( q = 0; q< N; q++)  {
            for( p = 0; p< N; p++)  {
                sum[r][q][p] = 0.0;
                for( s = 0; s< N; s++)  {
                    sum[r][q][p] = sum[r][q][p] + A[r][q][s]*C4[s][p];
                }
            }
            for( p = 0; p< N; p++)  {
                A[r][q][p] = sum[r][q][p];
            }
        }
    }
#pragma endscop

}


void print_array()
{
  int i, j, k;

  for (i=0; i<NMAX; i++) {
    for (j=0; j<NMAX; j++) {
      for (k=0; k<NMAX; k++) {
        fprintf(stdout, "%0.2lf ", A[i][j][k]);
        if (j%80 == 20) fprintf(stdout, "\n");
      }
    }
  }
  fprintf(stdout, "\n");
}

double t_start, t_end, t_acc;

int main()
{
  int i, j, k, t;
  long N=NMAX;

  t_acc = 0.0;

  for (t=0; t<1000; t++)  {

    for (i=0; i<N; i++) {
      for (j=0; j<N; j++) {
        C4[i][j] = (i*i+j*j)/((double)2*N*N);
        for (k=0; k<N; k++) {
          A[i][j][k] = (i*i+k*j)/((double)2*N*N);
        }
      }
    }

    IF_TIME(t_start = rtclock());

    doitgen(N);

    IF_TIME(t_end = rtclock());
    IF_TIME(t_acc += t_end - t_start);
  }

  IF_TIME(printf("Elapsed Time=%0.6lf\n", t_acc));

  if (fopen(".test", "r")) {
    print_array();
  }

  return 0;
}
