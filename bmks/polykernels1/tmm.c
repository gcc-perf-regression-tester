#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#ifdef SELFTEST
	#define NMAX 500
#else
	#define NMAX 3000
	#define MEASURE_TIME 1
#endif // SELFTEST

#define A_SIZE NMAX
#define B_SIZE NMAX
static double A[A_SIZE][A_SIZE];
static double B[B_SIZE][B_SIZE];
static double C[B_SIZE][B_SIZE];
//#ifdef SELFTEST
static double CC[B_SIZE][B_SIZE];
//#endif // SELFTEST

void tmm(long Ni, long Nj, long Nk) {
	int i, j, k;
	
	for(i = 0; i < Ni; i++) {
		for(j=i; j < Nj; j++) { 
			for(k=i;  k< Nk; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

}

#ifdef SELFTEST
void ref(N) {
	int i, j, k;
	
	for(i = 0; i < N; i++) {
		for(j=i; j < N; j++) { 
			for(k=i;  k< N; k++) {
				CC[i][j] += A[i][k] * B[k][j];
			}
		}
	}

}
#endif

int main(int argc, char *argv)
{
	struct timeval start;
	struct timeval end;
	long N=NMAX;
	int i, j, k;
	int errors = 0;

//#ifdef SELFTEST
	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			C[i][j] = CC[i][j] = 0;
			A[i][j] = B[i][j] = i+j;
		}
	}
//#endif

	gettimeofday(&start, NULL);
	tmm(N,N,N);
	gettimeofday(&end, NULL);

#ifdef SELFTEST
	ref(N);

	for (i = 0; i < B_SIZE; i++) {
		for (j = 0; j < B_SIZE; j++) {
			if (fabs(C[i][j]/CC[i][j] -1) > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("Error: expected CC[%d][%d] = %lf found C[%d][%d] = %lf\t=>\tabs(a/b -1) = %lf\n", 
							i, j, CC[i][j], i, j, C[i][j], fabs(CC[i][j]/C[i][j] -1) );
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: TMM ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif

#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif

  return (errors != 0 );
}
