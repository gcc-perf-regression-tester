#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#ifdef SELFTEST
   #define NMAX 100
#else
	#define MEASURE_TIME 1
  #define NMAX 1000
#endif // SELFTEST 


#define A_SIZE NMAX
static double A[A_SIZE][A_SIZE];
static double AA[A_SIZE][A_SIZE];

void lud(long N) {
	int i, j, k;
	
	for(k = 0; k < N; k++) {
		for(i=k+1; i<N; i++) { 
			A[k][i] =  A[k][i] / A[k][k];
		}
		for(i=k+1; i<N; i++) { 
			for(j=k+1; j<N; j++) { 
				A[i][j] = A[i][j] - A[i][k] * A[k][j];
			}
		}
	}

}

#ifdef SELFTEST
void ref(long N) {
	int i, j, k;
	
	for(k = 0; k < N; k++) {
		for(i=k+1; i<N; i++) { 
			AA[k][i] = AA[k][i] / AA[k][k];
		}
		for(i=k+1; i<N; i++) { 
			for(j=k+1; j<N; j++) { 
				AA[i][j] = AA[i][j] - AA[i][k] * AA[k][j];
			}
		}
	}

}
#endif // SELFTEST

int main(int argc, char *argv)
{
	
	struct timeval start;
	struct timeval end;
	long N = NMAX;
	int i, j;
	int errors = 0;

#if SELFTEST
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			A[i][j] = AA[i][j] = (rand()/(double)RAND_MAX);
		}
	} 
	// Make it diagonally dominant to make the computation numerically more stable
	for (i=0; i < N; i++) {
		A[i][i] = AA[i][i] = 4096;
  }
#endif // SELFTEST
	
	gettimeofday(&start, NULL);
	lud(N);
	gettimeofday(&end, NULL);
	
#ifdef  SELFTEST
	ref(N);

	// Ln:
	// The condition for error is relaxed here from abs-diff > 10e-8 to 
	// abs-diff > 10e-2 to account for the numerically unstable behavior
	// of the LUD computation. Since the initialized values are between 0 and 1, 
	// abs(a-b) is better measure than abs(a/b -1).
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if ( fabs(A[i][j] - AA[i][j]) > 10e-2)  {
				errors++;
#ifdef ST_VERB
				printf("ERROR: expected AA[%d][%d] = %lf found A[%d][%d] = %lf\t=>\t abs(a-b) = %lf\n", 
							i, j, AA[i][j], i, j, A[i][j], fabs(A[i][j]-AA[i][j]));
			} else {
				printf("AA[%d][%d] = %lf and A[%d][%d] = %lf\n", i, j, AA[i][j], i, j, A[i][j]);
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: LUD ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif // SELFTEST

#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
	return errors != 0;
}
