#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#ifdef SELFTEST
	#define NMAX 200
#else
	#define NMAX 3000
	#define MEASURE_TIME 1
#endif // SELFTEST

static double a[NMAX][NMAX], b[NMAX][NMAX], c[NMAX][NMAX];
static double cc[NMAX][NMAX];

void ssyr2k(long N) {
	int i,j,k;
	
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=j; k<N; k++) {
				c[j][k] += a[i][j] * b[i][k] + b[i][j] * a[i][k];
			}
		}
	}
}

#ifdef SELFTEST
void ref(long N) {
	int i,j,k;
	
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=j; k<N; k++) {
				cc[j][k] += a[i][j] * b[i][k] + b[i][j] * a[i][k];
			}
		}
	}
}
#endif 

int main()
{
	struct timeval start;
	struct timeval end;
    long N=NMAX;
	int i,j;
	int errors = 0;

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			c[i][j] = 0.0;
			cc[i][j] = 0.0;
			a[i][j] = i*j*32.5;
			b[i][j] = i*j*3.4565;
		}
	}

	gettimeofday(&start, NULL);
    ssyr2k(N);
	gettimeofday(&end, NULL);

#ifdef SELFTEST
	ref(N);

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			if ( fabs(c[i][j]/cc[i][j] -1) > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("Error: expected cc[%d][%d] = %lf found c[%d][%d] = %lf\t=>\tabs(a/b -1) = %lf\n", 
							i, j, cc[i][j], i, j, c[i][j], fabs(cc[i][j]/c[i][j] -1));
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: SSYR2K ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif

#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
    return (errors != 0);
}
