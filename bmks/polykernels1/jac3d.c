#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>


//#define SELFTEST 1
#define MEASURE_TIME 1

#define TMAX 100
#define NMAX 100

static double a[NMAX][NMAX][NMAX], b[NMAX][NMAX][NMAX];
#ifdef SELFTEST
static double aa[NMAX][NMAX][NMAX], bb[NMAX][NMAX][NMAX];
#endif

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void jac3d(long T, long N) {

    int t, i, j, k;
	double f1 = 1/7;
	double f2 = 1/7;

    for (t=0; t<T; t++){
        for (i=1; i<N-1; i++){
            for (j=1; j<N-1; j++){
                for (k=1; k<N-1; k++){
                    b[i][j][k] = f1*a[i][j][k] + f2*(a[i+1][j][k] + a[i-1][j][k] + a[i][j+1][k]
                            + a[i][j-1][k] + a[i][j][k+1] + a[i][j][k-1]);
                }
            }
        }
        for (i=1; i<N-1; i++){
            for (j=1; j<N-1; j++){
                for (k=1; k<N-1; k++){
                    a[i][j][k] = b[i][j][k];
                }
            }
        }
    }
} // end jac()

///////////////////////////////////////////////////////////////
// This version is not optimized by poly. This is a reference
//  function used for correctness checking.
#ifdef SELFTEST
void ref(long T, long N) {

    int t, i, j, k;
	double f1 = 1/7;
	double f2 = 1/7;

    for (t=0; t<T; t++){
        for (i=1; i<N-1; i++){
            for (j=1; j<N-1; j++){
                for (k=1; k<N-1; k++){
                    bb[i][j][k] = f1*aa[i][j][k] + f2*(aa[i+1][j][k] + aa[i-1][j][k] + aa[i][j+1][k]
                            + aa[i][j-1][k] + aa[i][j][k+1] + aa[i][j][k-1]);
                }
            }
        }
        for (i=1; i<N-1; i++){
            for (j=1; j<N-1; j++){
                for (k=1; k<N-1; k++){
                    aa[i][j][k] = bb[i][j][k];
                }
            }
        }
    }
} // end ref()
#endif 

int main()
{
	struct timeval start;
	struct timeval end;
	long T, N;
	int i,j;
	int errors=0;
	T = TMAX; N=NMAX;

///////////////////////////////////////////////////////////////
// Intialize if we are going to test for correctness
#ifdef SELFTEST
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            for (k=0; k<N; k++) {
                a[i][j][k] = aa[i][j][k] = (i+j+((double)k))/N;
            }
        }
    }
#endif 
///////////////////////////////////////////////////////////////


	// Compute Jacobi relaxation.
	gettimeofday(&start, NULL);
	jac3d(T,N);
	gettimeofday(&end, NULL);


///////////////////////////////////////////////////////////////
// Compare the results for correctness.
#ifdef SELFTEST

	printf("SELFTEST\n");
	// compute results using the non-poly-optimized ref version
	ref(T,N);
	// Compare results
	for (i = 1; i < N - 1; i++) {
	    for (j = 1; j < N - 1; j++) {
			for (k = 1; k < N - 1; k++) {
				if (b[i][j] != bb[i][j]) {
					errors++;
					printf("expected bb[%d][%d][%d] = %Lf found b[%d][%d][%d] = %Lf\t=>\t10^15*Diff is: %Lf\n", 
								i, j, k, bb[i][j][k], i, j, k, b[i][j][k], (bb[i][j][k] - b[i][j][k]) * 1000000000000000.0);
				}
            }
		}
	}
  if(errors != 0) printf("%d errors, FAILURE\n", errors);
  else printf("SUCCESS\n");  

#endif 
///////////////////////////////////////////////////////////////

#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif




  return errors != 0;
}
