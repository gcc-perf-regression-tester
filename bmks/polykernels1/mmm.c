#include <stdio.h>
#include <math.h>
#include <sys/time.h>

//#include "plog_macro.h"

//#define SELFTEST 1

#ifdef SELFTEST
	#define NMAX 400
#else
	#define MEASURE_TIME 1
	#define NMAX 4000
#endif // SELFTEST

#define I_SIZE NMAX
#define J_SIZE NMAX
#define K_SIZE NMAX

static double A[I_SIZE][K_SIZE];
static double B[K_SIZE][J_SIZE];
static double C[I_SIZE][J_SIZE];

#ifdef SELFTEST
static double CC[I_SIZE][J_SIZE];
#endif // SELFTEST

void mmm(long Ni, long Nj, long Nk) {
	int i, j, k;
	
	for(i = 0; i < Ni; i++) {
		for(j=0; j < Nj; j++) {
			for(k=0; k < Nk; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}

}


#ifdef SELFTEST
void ref(long Ni, long Nj, long Nk) {
	int i, j, k;
	
	for(i = 0; i < Ni; i++) {
		for(j=0; j < Nj; j++) {
			for(k=0; k < Nk; k++) {
				CC[i][j] += A[i][k] * B[k][j];
			}
		}
	}

}
#endif // SELFTEST

int main()
{
	struct timeval start;
	struct timeval end;
	long Ni=I_SIZE;
	long Nj=J_SIZE;
	long Nk=K_SIZE;
	int i, j, k;
	int errors = 0;

#ifdef SELFTEST
	for (i = 0; i < I_SIZE; i++) {
		for (j = 0; j < J_SIZE; j++) {
			C[i][j] = CC[i][j] = 0;
		}
	}

	for (i = 0; i < I_SIZE; i++) {
		for (k = 0; k < K_SIZE; k++) {
			A[i][k] = i+k*3.234;
		}
	}

	for (k = 0; k < K_SIZE; k++) {
		for (j = 0; j < J_SIZE; j++) {
			B[k][j] = j+k*2.342;
		}
	}
#endif // SELFEST

	gettimeofday(&start, NULL);
	mmm(Ni, Nj, Nk);
	gettimeofday(&end, NULL);

#ifdef SELFTEST
	ref(Ni, Nj, Nk);

	for (i = 0; i < I_SIZE; i++) {
		for (j = 0; j < J_SIZE; j++) {
			if ( fabs(C[i][j]/CC[i][j] -1) > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("expected CC[%d][%d] = %lf found C[%d][%d] = %lf\t=>\t abs(a/b -1) %lf\n", 
							i, j, CC[i][j], i, j, C[i][j], fabs(CC[i][j]/C[i][j] - 1) );
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: MMM ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif

#ifdef  MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif

  return errors != 0;
}
