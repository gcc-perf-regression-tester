#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#define SELFTEST 1

#include <sys/time.h>

#ifdef SELFTEST
  #define NMAX 1000
  #define TMAX 1000
#else
	#define MEASURE_TIME 1
  #define NMAX 50000
  #define TMAX 50000
#endif // SELFTEST


static double e[NMAX], h[NMAX];
static double ee[NMAX], hh[NMAX];

void fdtd1d(long N, long T) {

	int t,i;
	
	for (t=0; t < T; t++) {
		for (i=1; i < N-1; i++) {
			e[i] -= 0.5*(h[i]-h[i-1]);
		}
		for (i=0; i < N-1; i++) {
			h[i] -= 0.7*(e[i+1]-e[i]);
		}
	}

}

#if SELFTEST
void ref(long N, long T) {

	int t,i;
	
	for (t=0; t < T; t++) {
		for (i=1; i < N-1; i++) {
			ee[i] -= 0.5*(hh[i]-hh[i-1]);
		}
		for (i=0; i < N-1; i++) {
			hh[i] -= 0.7*(ee[i+1]-ee[i]);
		}
	}

}
#endif

int main()
{
	struct timeval start;
	struct timeval end;
	long N=NMAX;
	long T=TMAX;
	int i;
	int errors = 0;

#if SELFTEST
	for (i = 0; i < NMAX; i++) {
		e[i] = ee[i] = i;
		h[i] = hh[i] = i*0.5;
	}
#endif


	gettimeofday(&start, NULL);
	fdtd1d(N,T);
	gettimeofday(&end, NULL);

#if SELFTEST
	ref(N,T);

	for (i = 0; i < NMAX; i++) {
		if ( fabs(e[i]-ee[i]) > 10e-8) {
			errors++;
#ifdef ST_VERB
			printf("ERROR: expected ee[%d] = %lf found e[%d] = %lf\t=>abs(a/b -1) is: %lf\n", 
						i, ee[i], i, e[i], fabs(ee[i]/e[i] -1));
#endif // ST_VERB
		}
		if ( fabs(h[i]-hh[i]) > 10e-8) {
			errors++;
#ifdef ST_VERB
			printf("expected hh[%d] = %lf found h[%d] = %lf\t=>abs(a/b -1) is: %lf\n", 
						i, hh[i], i, h[i], fabs(hh[i]/h[i] -1));
#endif // ST_VERB
		}
	}

	printf("SELFTEST: FDTD1D ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  

#endif // SELFTEST


#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
    return errors != 0;
}
