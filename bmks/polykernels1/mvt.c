#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "plog_macro.h"

// #define SELFTEST 1


#include <sys/time.h>

#ifdef SELFTEST
  #define NMAX 2000
#else
	#define MEASURE_TIME 1
  #define NMAX 15000
#endif

static double x1[NMAX], x2[NMAX], a[NMAX][NMAX], y_1[NMAX], y_2[NMAX];
#ifdef SELFTEST
static double Tx1[NMAX], Tx2[NMAX], Ta[NMAX][NMAX], Ty_1[NMAX], Ty_2[NMAX];
#endif

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void mvt(long N) {

    int i,j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            x1[i] = x1[i] + a[i][j] * y_1[j];
        }
    }
    
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            x2[i] = x2[i] + a[j][i] * y_2[j];
        }
    }
}// end mvt()

///////////////////////////////////////////////////////////////
// This version is not optimized by poly. This is a reference
//  function used for correctness checking.
#ifdef SELFTEST
void ref(long N) {

    int i,j;

    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            Tx1[i] = Tx1[i] + Ta[i][j] * Ty_1[j];
        }
    }
    
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            Tx2[i] = Tx2[i] + Ta[j][i] * Ty_2[j];
        }
    }
} // end ref()
#endif



int main()
{
	long N=NMAX;
	int i,j;
	int errors=0;
    struct timeval start;
    struct timeval end;


	
///////////////////////////////////////////////////////////////
// Intialize if we are going to test for correctness
#ifdef SELFTEST
	for (i = 0; i < N; i++) {
		x1[i] = x2[i] = Tx1[i] = Tx2[i] = i*0.5+2.3;
		y_1[i] = y_2[i] = Ty_1[i] = Ty_2[i] = i*0.5+2.3;
		for (j = 0; j < N; j++) {
			a[i][j] = Ta[i][j]  = i*j*0.5+2.3;
		}
	}
#endif 
///////////////////////////////////////////////////////////////
		

	// Compute MVT
	gettimeofday(&start, NULL);
        mvt(N);
	gettimeofday(&end, NULL);
#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
 	  (end.tv_sec - start.tv_sec + 
	  (double)(end.tv_usec - start.tv_usec)/1000000));
#endif



///////////////////////////////////////////////////////////////
// Compare the results for correctness.
#ifdef SELFTEST

	// compute results using the non-poly-optimized ref version
	ref(N);
	// Compare results
	for (i = 0; i < N; i++) {
		if (fabs(x1[i]/Tx1[i] -1) > 10e-8) {
			errors++;
#ifdef ST_VERB
			printf("expected Tx1[%d] = %lf found x1[%d] = %lf\t=>\t abs(a/b -1) =  %lf\n", 
						i, Tx1[i], i, x1[i], fabs(Tx1[i]/x1[i] - 1) );
#endif // ST_VERB
		}
	}
	for (i = 0; i < N; i++) {
		if ( fabs(x2[i]/Tx2[i] -1) > 10e-6) {
			errors++;
#ifdef ST_VERB
			printf("expected Tx2[%d] = %lf found x2[%d] = %lf\t=>\t abs(a/b - 1) =  %lf\n", 
						i, Tx2[i], i, x2[i], fabs(Tx2[i]/x2[i] -1) );
#endif // ST_VERB
		}
	}
	printf("SELFTEST: MVT ");
  if(errors != 0) printf("FAILED with %d errors!\n", errors);
  else printf("SUCCESS!\n");  
#endif 
///////////////////////////////////////////////////////////////


	return (errors != 0);
}
