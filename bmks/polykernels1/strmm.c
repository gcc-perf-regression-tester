#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#ifdef SELFTEST
	#define NMAX 150
#else
	#define NMAX 3000
	#define MEASURE_TIME 1
#endif // SELFTEST

static double a[NMAX][NMAX], b[NMAX][NMAX];
static double bb[NMAX][NMAX];

void strmm(long N) {
	int i,j,k;
	
	for (i=1; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=0; k<i; k++) {
				b[j][k] += a[i][k] * b[j][i];
			}
		}
	}
}

#ifdef SELFTEST
void ref(long N) {
	int i,j,k;
	
	for (i=1; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=0; k<i; k++) {
				bb[j][k] += a[i][k] * bb[j][i];
			}
		}
	}
}
#endif

int main()
{
	struct timeval start;
	struct timeval end;
    long N=NMAX;
	int i,j;
	int errors = 0;

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			b[i][j] = i+j;
			bb[i][j] = i+j;
			a[i][j] = i*j*0.5;
		}
	}

	gettimeofday(&start, NULL);
    strmm(N);
	gettimeofday(&end, NULL);

#ifdef SELFTEST
	ref(N);

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			if (fabs(b[i][j]/bb[i][j] -1) > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("Error: expected bb[%d][%d] = %lf found b[%d][%d] = %lf\t=>\tabs(a/b -1) = %lf\n", 
							i, j, bb[i][j], i, j, b[i][j], fabs(bb[i][j]/b[i][j] -1));
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: STRMM ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif

#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
    return (errors != 0);
}
