#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "plog_macro.h"

// #define SELFTEST 1

#include <sys/time.h>

#ifdef SELFTEST 
	#define TMAX 5000
	#define NMAX 5000
#else
	#define MEASURE_TIME 1
	#define TMAX 50000
	#define NMAX 50000
#endif 

static double a[NMAX], b[NMAX];
#ifdef SELFTEST
static double aa[NMAX], bb[NMAX];
#endif

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void jac(long T, long N) {

    int t, i, j;

    for (t = 0; t < T; t++) {
        for (i = 2; i < N - 1; i++) {
            b[i] = ((double)333/1000) * (a[i - 1] + a[i] + a[i + 1]); // S1
        }
        for (j = 2; j < N - 1; j++) {
            a[j] = b[j]; // S2
        }
    }
} // end jac()

///////////////////////////////////////////////////////////////
// This version is not optimized by poly. This is a reference
//  function used for correctness checking.
#ifdef SELFTEST
void ref(long T, long N) {

    int t, i, j;

    for (t = 0; t < T; t++) {
        for (i = 2; i < N - 1; i++) {
            bb[i] = ((double)333/1000) * (aa[i - 1] + aa[i] + aa[i + 1]); // S1
        }
        for (j = 2; j < N - 1; j++) {
            aa[j] = bb[j]; // S2
        }
    }
} // end ref()
#endif 

int main()
{
    long T, N;
    int t,i;
    int errors=0;
    struct timeval start;
    struct timeval end;

    T = TMAX; N=NMAX;

///////////////////////////////////////////////////////////////
// Intialize if we are going to test for correctness
#ifdef SELFTEST
	for (i = 2; i < N - 1; i++) {
		a[i] = aa[i] = b[i] = bb[i] = t*i*0.5;
	}
#endif 
///////////////////////////////////////////////////////////////


	// Compute Jacobi relaxation.
	gettimeofday(&start, NULL);
	jac(T,N);
	gettimeofday(&end, NULL);
#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
 	  (end.tv_sec - start.tv_sec + 
	  (double)(end.tv_usec - start.tv_usec)/1000000));
#endif


///////////////////////////////////////////////////////////////
// Compare the results for correctness.
#ifdef SELFTEST

	// compute results using the non-poly-optimized ref version
	ref(T,N);
	// Compare results
	for (i = 2; i < N - 1; i++) {
		if (fabs(b[i]/bb[i] -1) > 10e-8) {
			errors++;
#ifdef ST_VERB
			printf("Error: expected bb[%d] = %lf found b[%d] = %lf\t=>\t|a/b -1| is: %lf\n", 
						i, bb[i], i, b[i], fabs(bb[i]/b[i] -1));
#endif // ST_VERB
		}
	}
	printf("SELFTEST: JAC1D ");
  if(errors != 0) printf("FAILED with %d errors.\n", errors);
  else printf("SUCCESS\n");  

#endif 
///////////////////////////////////////////////////////////////
	return (errors != 0);

}
