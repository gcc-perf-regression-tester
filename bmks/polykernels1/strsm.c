#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#ifdef SELFTEST
	#define NMAX 150
#else
	#define NMAX 3000
	#define MEASURE_TIME 1
#endif // SELFTEST

static double a[NMAX][NMAX], b[NMAX][NMAX];
static double bb[NMAX][NMAX];

void strsm(long N) {
	int i,j,k;
	
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=i+1; k<N; k++) {
				if (k == i+1) b[j][i] /= a[i][i];
				b[j][k] -= a[i][k] * b[j][i];
			}
		}
	}
}

#ifdef SELFTEST
void ref(long N) {
	int i,j,k;
	
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			for (k=i+1; k<N; k++) {
				if (k == i+1) bb[j][i] /= a[i][i];
				bb[j][k] -= a[i][k] * bb[j][i];
			}
		}
	}
}
#endif

int main()
{
	struct timeval start;
	struct timeval end;
	long N=NMAX;
	int i,j;
	int errors = 0;

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			b[i][j] = i*j*0.3;
			bb[i][j] = i*j*0.3;
			a[i][j] = i+j+1;
		}
	}

	gettimeofday(&start, NULL);
	strsm(N);
	gettimeofday(&end, NULL);



#ifdef SELFTEST
	ref(N);

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			if (fabs(b[i][j]/bb[i][j] -1)  > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("Error: expected bb[%d][%d] = %lf found b[%d][%d] = %lf\t=>\tabs(a/b -1) = %lf\n", 
							i, j, bb[i][j], i, j, b[i][j], fabs(bb[i][j]/b[i][j] -1));
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: STRSM ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif

#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
    return (errors != 0);
}
