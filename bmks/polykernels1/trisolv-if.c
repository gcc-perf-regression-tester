#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#ifdef SELFTEST
	#define NMAX 150
#else
	#define NMAX 3000
	#define MEASURE_TIME 1
#endif // SELFTEST

// #define S1(i,j,k) B[j][i]=B[j][i]-L[j][k]*B[k][i]
// #define S2(i,j) B[j][i]=B[j][i]/L[j][j]

static double B[NMAX][NMAX], L[NMAX][NMAX];
static double BB[NMAX][NMAX];

void trisolv(long N) {

    long i,j,k;

    for (i=0;i<=N-1;i++) {

        for (j=0;j<=N-1;j++) {

            //for (k=0;k<=j-1;k++) {
            for (k=0;k<=j;k++) {

                if (k<=j-1) 
                    {B[j][i]=B[j][i]-L[j][k]*B[k][i];}  //S1 ;
                if ( k == j ) 
                    {B[j][i]=B[j][i]/L[j][j];} // S2 ;

            }

            //B[j][i]=B[j][i]/L[j][j]; // S2 ;

        } // for j

    } // for i

} // end trisolv()

#ifdef SELFTEST
void ref(long N) {

    long i,j,k;

    for (i=0;i<=N-1;i++) {

        for (j=0;j<=N-1;j++) {

            for (k=0;k<=j-1;k++) {

                BB[j][i]=BB[j][i]-L[j][k]*BB[k][i];  //S1 ;

            }

            BB[j][i]=BB[j][i]/L[j][j]; // S2 ;

        } // for j

    } // for i
}
#endif // SELFTEST

int main()
{
	struct timeval start;
	struct timeval end;
    long N=NMAX;
	int i,j;
	int errors = 0;

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			B[i][j]  = BB[i][j] = i+j;
		    L[i][j] = (i+j+3.45) *i*j*0.5;
		}
	}

	gettimeofday(&start, NULL);
    trisolv(N);
	gettimeofday(&end, NULL);

#ifdef SELFTEST
	ref(N);

	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX ; j++) {
			if (fabs(B[i][j]/BB[i][j] -1) > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("Error: expected BB[%d][%d] = %lf found B[%d][%d] = %lf\t=>\tabs(a/b -1) = %lf\n", 
							i, j, BB[i][j], i, j, B[i][j], fabs(BB[i][j]/B[i][j] -1) );
#endif // ST_VERB
			}
		}
	}

	printf("SELFTEST: TRISOLV ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
	else printf("SUCCESS!\n");  
#endif

#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif
    return (errors != 0);
}
