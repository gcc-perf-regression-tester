#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#include "plog_macro.h"

//#define SELFTEST 1

#ifdef SELFTEST
	#define TMAX 150
	#define NMAX 150
#else 
	#define MEASURE_TIME 1
	#define TMAX 1500
	#define NMAX 1500
#endif // SELFTEST

static double a[NMAX][NMAX], b[NMAX][NMAX];
#ifdef SELFTEST
static double aa[NMAX][NMAX], bb[NMAX][NMAX];
#endif

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void jac2d(long T, long N) {

    int t, i, j;

    for (t = 0; t < T; t++) {
        for (i = 1; i < N - 1; i++) {
            for (j = 1; j < N-1; j++) {
                b[i][j] = (
                            a[i-1][j-1] + a[i-1][j] + a[i-1][j+1] +
                            a[i][j-1]   + a[i][j]   + a[i][j+1] + 
                            a[i+1][j-1] + a[i+1][j] + a[i+1][j+1]
			) / 9.0; // S1
            }
        }
        for (i = 1; i < N - 1; i++) {
           for (j = 1; j < N-1; j++) {
              a[i][j] = b[i][j]; // S2
           }
        }
    }
} // end jac()

///////////////////////////////////////////////////////////////
// This version is not optimized by poly. This is a reference
//  function used for correctness checking.
#ifdef SELFTEST
void ref(long T, long N) {

    int t, i, j;

    for (t = 0; t < T; t++) {
        for (i = 1; i < N - 1; i++) {
            for (j = 1; j < N-1; j++) {
                bb[i][j] = (
                            aa[i-1][j-1] + aa[i-1][j] + aa[i-1][j+1] +
                            aa[i][j-1]   + aa[i][j]   + aa[i][j+1] +
                            aa[i+1][j-1] + aa[i+1][j] + aa[i+1][j+1]
			) / 9.0; // S1
            }
        }
        for (i = 1; i < N - 1; i++) {
           for (j = 1; j < N-1; j++) {
              aa[i][j] = bb[i][j]; // S2
           }
        }
    }
} // end ref()
#endif 

int main()
{
	struct timeval start;
	struct timeval end;
	long T, N;
	int i,j;
	int errors=0;
	T = TMAX; N=NMAX;

///////////////////////////////////////////////////////////////
// Intialize if we are going to test for correctness
#ifdef SELFTEST
	for (i = 0; i < N; i++) {
	    for (j = 0; j < N; j++) {
					a[i][j] = aa[i][j] = i*j*0.5;
          b[i][j] = bb[i][j] = 0;
	    }
	}
#endif 
///////////////////////////////////////////////////////////////


	// Compute Jacobi relaxation.
	gettimeofday(&start, NULL);
	jac2d(T,N);
	gettimeofday(&end, NULL);


///////////////////////////////////////////////////////////////
// Compare the results for correctness.
#ifdef SELFTEST

	// compute results using the non-poly-optimized ref version
	ref(T,N);
	// Compare results
	for (i = 1; i < N - 1; i++) {
	    for (j = 1; j < N - 1; j++) {
		if ( fabs(b[i][j]/bb[i][j] -1) > 10e-8) {
			errors++;
#ifdef ST_VERB
			printf("Error: expected bb[%d][%d] = %lf found b[%d][%d] = %lf\t=>\tabs(a/b -1) is: %lf\n", 
						i, j, bb[i][j], i, j, b[i][j], fabs(bb[i][j]/b[i][j] -1));
#endif // ST_VERB
		}
            }
	}
	printf("SELFTEST: JAC2D ");
  if(errors != 0) printf("FAILED with %d errors!\n", errors);
  else printf("SUCCESS!\n");  

#endif 
///////////////////////////////////////////////////////////////

#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif

  return errors != 0;
}
