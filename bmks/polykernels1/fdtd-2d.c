#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <unistd.h>
#include <sys/time.h>

#define MEASURE_TIME 1

#define TMAX 500
#define NX 2000
#define NY 2000

double ex[NX][NY+1];
double ey[NX+1][NY];
double hz[NX][NY];


void fdtd2d(long nx, long ny, long tmax)
{
  int t, i, j;

  for(t=0; t<tmax; t++)  {
    for (j=0; j<ny; j++)
      ey[0][j] = t;
    for (i=1; i<nx; i++)
      for (j=0; j<ny; j++)
        ey[i][j] = ey[i][j] - 0.5*(hz[i][j]-hz[i-1][j]);
    for (i=0; i<nx; i++)
      for (j=1; j<ny; j++)
        ex[i][j] = ex[i][j] - 0.5*(hz[i][j]-hz[i][j-1]);
    for (i=0; i<nx; i++)
      for (j=0; j<ny; j++)
        hz[i][j]=hz[i][j]-0.7*(ex[i][j+1]-ex[i][j]+ey[i+1][j]-ey[i][j]);
  }
}


int main()
{
  int t, i, j, k, l, m, n;

	struct timeval start;
	struct timeval end;

  for (i=0; i<NX+1; i++)  {
    for (j=0; j<NY; j++)  {
      ey[i][j] = 0;
    }
  }

  for (i=0; i<NX; i++)  {
    for (j=0; j<NY+1; j++)  {
      ex[i][j] = 0;
    }
  }

  for (j=0; j<NY; j++)  {
    ey[0][j] = ((double)j)/NY;
  }

  for (i=0; i<NX; i++)    {
    for (j=0; j<NY; j++)  {
      hz[i][j] = 0;
    }
  }

  long nx = NX;
  long ny = NY;
  long tmax = TMAX;

  gettimeofday(&start, NULL);
  fdtd2d(nx,ny,tmax);
  gettimeofday(&end, NULL);

  printf("%lf \n", hz[NX-1][NY-1]);

#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
			(end.tv_sec - start.tv_sec + 
			(double)(end.tv_usec - start.tv_usec)/1000000));
#endif

  return hz[NX-1][NY-1];
}
