#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "plog_macro.h"

// #define SELFTEST 1

#include <sys/time.h>

#ifdef SELFTEST

#define TMAX 100
#define NMAX 100

#else

#define MEASURE_TIME 1
#define TMAX 1000
#define NMAX 3000

#endif // SELFTEST

// +13, +23, +37
static double X[NMAX][NMAX], A[NMAX][NMAX], B[NMAX][NMAX];
#ifdef SELFTEST
static double TX[NMAX][NMAX], TA[NMAX][NMAX], TB[NMAX][NMAX];
#endif

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void adi(long T, long N) {

    int t, i1, i2;

    for (t = 0; t < T; t++) {

        // Column Sweep
        for (i1=0; i1<N; i1++) {
            for (i2 = 1; i2 < N; i2++) {
                X[i1][i2] = X[i1][i2] - (X[i1][i2-1] * A[i1][i2]) / B[i1][i2-1];
                B[i1][i2] = B[i1][i2] - (A[i1][i2] * A[i1][i2]) / B[i1][i2-1];
            }
        }

        // Row Sweep
        for (i1=1; i1<N; i1++) {
            for (i2 = 0; i2 < N; i2++) {
                X[i1][i2] = X[i1][i2] - (X[i1-1][i2] * A[i1][i2]) / B[i1-1][i2];
                B[i1][i2] = B[i1][i2] - (A[i1][i2] * A[i1][i2]) / B[i1-1][i2];
            }
        }

    } // end of t loop
}

///////////////////////////////////////////////////////////////
// This version is not optimized by poly. This is a reference
//  function used for correctness checking.
#ifdef SELFTEST
void ref(long T, long N) {

    int t, i1, i2;

    for (t = 0; t < T; t++) {

        // Column Sweep
        for (i1=0; i1<N; i1++) {
            for (i2 = 1; i2 < N; i2++) {
                TX[i1][i2] = TX[i1][i2] - (TX[i1][i2-1] * TA[i1][i2]) / TB[i1][i2-1];
                TB[i1][i2] = TB[i1][i2] - (TA[i1][i2] * TA[i1][i2]) / TB[i1][i2-1];
            }
        }

        // Row Sweep
        for (i1=1; i1<N; i1++) {
            for (i2 = 0; i2 < N; i2++) {
                TX[i1][i2] = TX[i1][i2] - (TX[i1-1][i2] * TA[i1][i2]) / TB[i1-1][i2];
                TB[i1][i2] = TB[i1][i2] - (TA[i1][i2] * TA[i1][i2]) / TB[i1-1][i2];
            }
        }

    } // end of t loop
}
#endif

int main()
{
    long T=TMAX, N=NMAX;
    int i,j;
    int errors = 0;
    struct timeval start;
    struct timeval end;


///////////////////////////////////////////////////////////////
// Intialize if we are going to test for correctness
#ifdef SELFTEST
	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			X[i][j] = TX[i][j] = i+j+100.23;
			A[i][j] = TA[i][j] = i*j*3.23;
			B[i][j] = TB[i][j]  = i*j* (i+j) + 55.5;
		}
	}
#endif 
///////////////////////////////////////////////////////////////


		// Compute the ADI
    gettimeofday(&start, NULL);
    adi(T,N);
    gettimeofday(&end, NULL);
#if MEASURE_TIME
    printf("Elapsed Time=%lf\n", 
 	  (end.tv_sec - start.tv_sec + 
	  (double)(end.tv_usec - start.tv_usec)/1000000));
#endif


///////////////////////////////////////////////////////////////
// Compare the results for correctness.
#ifdef SELFTEST

	// compute results using the non-poly-optimized ref version
	ref(T,N);

	// Compare results
	for (i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			if ( fabs( (X[i][j]/TX[i][j]) -1) > 10e-8 )  {
				errors++;
#ifdef ST_VERB
				printf("expected TX[%d][%d] = %lf found X[%d][%d] = %lf\t=>\t Error: abs(a/b -1) = %lf\n",
							i,j, TX[i][j], i,j, X[i][j], fabs( (X[i][j]/TX[i][j]) -1));
			} else {
				printf("TX[%d][%d] = %lf and X[%d][%d] = %lf\n", i,j, TX[i][j], i,j, X[i][j]);
#endif // ST_VERB

			}
			if ( fabs( (B[i][j]/TB[i][j]) -1) > 10e-8)  {
				errors++;
#ifdef ST_VERB
				printf("expected TB[%d][%d] = %lf found B[%d][%d] = %lf\t=>\t Error: abs(a/b -1) =  %lf\n",
							i,j, TB[i][j], i,j, B[i][j], fabs( (B[i][j]/TB[i][j]) -1));
			} else {
				printf("TB[%d][%d] = %lf and B[%d][%d] = %lf\n", i,j, TB[i][j], i,j, B[i][j]);
#endif // ST_VERB

			}
		}
	}
  
	printf("SELFTEST: ADI ");
	if(errors != 0) printf("FAILED with %d errors!\n", errors);
  else printf("SUCCESS!\n");  

#endif 
///////////////////////////////////////////////////////////////
    return (errors != 0);
}
