#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "plog_macro.h"

// #define SELFTEST 1

#include <sys/time.h>

#ifdef SELFTEST
	#define NMAX 250
	#define TMAX 50
#else
	#define MEASURE_TIME 1
	#define NMAX 2500
	#define TMAX 500
#endif // SELFTEST


static double a[NMAX][NMAX];
#ifdef SELFTEST
static double Ta[NMAX][NMAX];
#endif

///////////////////////////////////////////////////////////////
// This is the function optimized by poly
void gs(long N, long T) {

    long i,j,t;

    for (t=0; t<=T-1; t++)  {
        for (i=1; i<=N-2; i++)  {
            for (j=1; j<=N-2; j++)  {
                a[i][j] = (a[i-1][j-1] + a[i-1][j] + a[i-1][j+1] 
                        + a[i][j-1] + a[i][j] + a[i][j+1]
                        + a[i+1][j-1] + a[i+1][j] + a[i+1][j+1])/9.0;
            }
        }
    }
}

///////////////////////////////////////////////////////////////
// This version is not optimized by poly. This is a reference
//  function used for correctness checking.
#ifdef SELFTEST
void ref(long N, long T) {

    long i,j,t;

    for (t=0; t<=T-1; t++)  {
        for (i=1; i<=N-2; i++)  {
            for (j=1; j<=N-2; j++)  {
                Ta[i][j] = (Ta[i-1][j-1] + Ta[i-1][j] + Ta[i-1][j+1] 
                        + Ta[i][j-1] + Ta[i][j] + Ta[i][j+1]
                        + Ta[i+1][j-1] + Ta[i+1][j] + Ta[i+1][j+1])/9.0;
            }
        }
    }
}// end ref()

#endif

int main()
{
    int i,j;
    int errors = 0;
    struct timeval start;
    struct timeval end;


///////////////////////////////////////////////////////////////
// Intialize if we are going to test for correctness
#ifdef SELFTEST
	for (i = 0; i < NMAX; i++) {
		for (j = 0; j < NMAX; j++) {
			a[i][j] = Ta[i][j]  = i*j*0.5;
		}
	}
#endif 
///////////////////////////////////////////////////////////////



	// Compute stencil
	gettimeofday(&start, NULL);
	gs(NMAX,TMAX);
	gettimeofday(&end, NULL);
#if MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
 	  (end.tv_sec - start.tv_sec + 
	  (double)(end.tv_usec - start.tv_usec)/1000000));
#endif



///////////////////////////////////////////////////////////////
// Compare the results for correctness.
#ifdef SELFTEST

	// compute results using the non-poly-optimized ref version
	ref(NMAX,TMAX);
	// Compare results
	for (i=0; i<NMAX; i++) {
		for(j=0; j<NMAX; j++) {
			if (fabs(a[i][j]/Ta[i][j] -1) > 10e-8)  {
				errors++;
#ifdef ST_VERB
				printf("expected Ta[%d][%d] = %lf found a[%d][%d] = %lf\t=>\t abs(a/b -1) = %lf\n",
							i,j, Ta[i][j], i,j, a[i][j], fabs(Ta[i][j]/a[i][j] -1));
#endif // ST_VERB
			}
		}
	}
	printf("SELFTEST: Gauss Seidel ");
  if(errors != 0) printf("FAILED with %d errors!\n", errors);
  else printf("SUCCESS!\n");  

#endif 
///////////////////////////////////////////////////////////////

	return errors != 0;

}
