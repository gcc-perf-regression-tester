#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#define MEASURE_TIME 1

#define NMAX 200

double A[NMAX][NMAX][NMAX];
double sum[NMAX][NMAX][NMAX];
double C4[NMAX][NMAX];

void doitgen(long N) {
	int r, q, p, s;

    for( r = 0; r< N; r++)  {
        for( q = 0; q< N; q++)  {
            for( p = 0; p< N; p++)  {
                sum[r][q][p] = 0;
                for( s = 0; s< N; s++)  {
                    sum[r][q][p] = sum[r][q][p] + A[r][q][s]*C4[s][p];
                }
            }
            for( p = 0; p< N; p++)  {
                A[r][q][p] = sum[r][q][p];
            }
        }
    }

}


int main()
{
	struct timeval start;
	struct timeval end;
  int i, j, k;
  long N=NMAX;
  int errors = 0;

  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      C4[i][j] = (i*i+j*j)/((double)N*N);
      for (k=0; k<N; k++) {
        A[i][j][k] = (i*i+j*j + k)/((double)N*N);
      }
    }
  }

  gettimeofday(&start, NULL);
  doitgen(N);
  gettimeofday(&end, NULL);

  printf("%lf \n", A[N-1][N-1][N-1]);

#if MEASURE_TIME
  printf("Elapsed Time=%lf\n",
      (end.tv_sec - start.tv_sec + 
       (double)(end.tv_usec - start.tv_usec)/1000000));
#endif

  return 0;
}
