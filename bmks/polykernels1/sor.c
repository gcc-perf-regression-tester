#include <stdio.h>
#include <math.h>
#include "plog_macro.h"


#include <sys/time.h>

#ifdef SELFTEST
	#define ARRAY_SIZE 1500
#else 
	#define ARRAY_SIZE 15000
	#define MEASURE_TIME 1
#endif  // SELFTEST


static double P[ARRAY_SIZE][ARRAY_SIZE];
#ifdef SELFTEST
static double PP[ARRAY_SIZE][ARRAY_SIZE];
#endif // SELFTEST

void sor(int N1, int N2){
  int i, j, k;

  for(i=1; i<N1-1; i++) {
    for(j=1; j<N2-1; j++) {
      P[i][j] = (P[i][j] + P[i][j-1] + P[i][j+1] + P[i-1][j] + P[i+1][j]) / 5;
    }
  }
}

#ifdef SELFTEST
void ref(int N1, int N2){
  int i, j, k;

  for(i=1; i<N1-1; i++) {
    for(j=1; j<N2-1; j++) {
      PP[i][j] = (PP[i][j] + PP[i][j-1] + PP[i][j+1] + PP[i-1][j] + PP[i+1][j]) / 5;
    }
  }
}
#endif // SELFTEST




int main(int argc, char *argv)
{
  int k,j,i;
  int errors = 0;
  int N1 = ARRAY_SIZE-1, N2 = ARRAY_SIZE-1;
    struct timeval start;
    struct timeval end;

  if(argc > 5){ 
    N1 = 12;
    N2 = 12;
  }

#ifdef SELFTEST
  for(i=0; i<ARRAY_SIZE; i++){
    for(j=0; j<ARRAY_SIZE; j++){
      P[i][j] = i*j*0.5;
      PP[i][j] = i*j*0.5;
    }
  }
#endif

	gettimeofday(&start, NULL);
        sor(N1, N2);
	gettimeofday(&end, NULL);
#ifdef MEASURE_TIME
	printf("Elapsed Time=%lf\n", 
 	  (end.tv_sec - start.tv_sec + 
	  (double)(end.tv_usec - start.tv_usec)/1000000));
#endif


#ifdef SELFTEST
  ref(N1, N2);

  for(i=0; i<ARRAY_SIZE; i++) {
    for(j=0; j<ARRAY_SIZE; j++) {
      if(fabs(P[i][j]/PP[i][j] -1) > 10e-8) {
				errors++;
#ifdef ST_VERB
				printf("Error: expected PP[%d][%d] = %lf found P[%d][%d] = %lf\t=>\tabs(a/b -1) %lf\n", 
							i, j, PP[i][j], i, j, P[i][j], fabs(PP[i][j]/P[i][j] -1));
#endif // ST_VERB
      }
    }
  }  

  printf("SELFTEST: SOR5 ");
  if(errors != 0) printf("FAILED with %d errors!\n", errors);
  else  printf("SUCCESS!\n");
#endif
  return (errors !=0);
}
