#!/bin/bash

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="pb05"
bmks="ac aermod air capacita channel doduc fatigue gas_dyn induct
linpk mdbx nf protein rnflow test_fpu tfft"

parse_nb () {
    if test $b = "nf"; then
	nb=`grep -m 3 $b $tag | tail -1 | awk '{ print $4 }'`
    else
	nb=`grep -m 2 $b $tag | tail -1 | awk '{ print $4 }'`
    fi
}

. $HOME/gcc/test/plot.sh
