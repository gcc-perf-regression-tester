#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="SPEC2006"
. $HOME/gcc/test/set_config.sh

if ! test -e $B/shrc; then
    echo "SPEC CPU2006 is not installed, skipping."
    exit 1
fi

cfg=$B/config/$config.cfg
> $cfg
echo "makeflags	= -j$procs" >> $cfg
cat >> $cfg <<EOF
action		= validate
size		= ref
iterations	= 1
tune		= base
output_format	= txt
check_md5	= 0
reportable	= 0
ignore_errors	= 1
teeout		= 0
teerunout	= 0
minimize_builddirs = 0
minimize_rundirs= 0

default=base=default:
EOF
echo "OPTIMIZE = $FLAGS" >> $cfg
echo "COPTIMIZE = $CFLAGS" >> $cfg
echo "CXXOPTIMIZE = $CXXFLAGS" >> $cfg
echo "FOPTIMIZE = $FFLAGS" >> $cfg
cat $B/rest_of_config.cfg >> $cfg

cd $B
. ./shrc
runspec --deletework --rebuild --config=$config.cfg all

number=`cat $B/result/CPU2006.lock`
catlog $B/result/CINT2006.${number}*.txt
catlog $B/result/CFP2006.${number}*.txt

. $X/finish_log.sh

