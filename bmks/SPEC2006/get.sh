#!/bin/bash

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

bmk="SPEC2006"
bmks="400.perlbench 401.bzip2 403.gcc 429.mcf 445.gobmk 456.hmmer
458.sjeng 462.libquantum 464.h264ref 471.omnetpp 473.astar
483.xalancbmk 410.bwaves 416.gamess 433.milc 434.zeusmp 435.gromacs
436.cactusADM 437.leslie3d 444.namd 447.dealII 450.soplex 453.povray
454.calculix 459.GemsFDTD 465.tonto 470.lbm 481.wrf 482.sphinx3"

parse_nb () {
    nb=`grep -m 2 $b $tag | tail -1 | awk '{ print $3 }'`
}

. $HOME/gcc/test/plot.sh
