#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

# This file is sourced at the beginning of every benchmark in the
# go.sh script.  It sets the CONFIG and FLAGS variables and outputs
# their values to the LOG file.

X=$HOME/gcc/test
B=$X/bmks/$bmk
config=$1

case "$config" in
    loop-interchange-block)
	FLAGS="-O3 -funroll-loops -fpeel-loops -ffast-math -march=native -floop-interchange -floop-block"
	;;
    O2)
	FLAGS="-O2 -march=native"
	;;
    lto)
	FLAGS="-O3 -funroll-loops -fpeel-loops -ffast-math -march=native -flto -fwhole-program -flto-partition=none"
	;;
    Ofast)
	FLAGS="-Ofast -march=native"
	;;
    ofast | "")
	FLAGS="-O3 -funroll-loops -fpeel-loops -ffast-math -march=native"
	config="ofast"
	;;
    *)
	echo "Config $config not handled."
	exit 0
	;;
esac

CFLAGS=$FLAGS
CXXFLAGS=$FLAGS
FFLAGS=$FLAGS

now=`date +"%Y_%m_%d_%H_%M_%S"`
tag="${bmk}_${config}_${now}"
log=$X/log/$tag
mkdir -p $X/log

echo "config file: $config" > $log
echo "FLAGS=$FLAGS" >> $log

catlog () {
    f=$1
    shift
    if test -e $f; then
	x=/tmp/catlog
	> $x
	echo "(cat $f" >> $x
	cat $f >> $x
	for i in `seq 1 $#`; do
	    eval y=\$$i
	    sed -i -e "/$y/d" $x
	done
	echo "tac $f)" >> $x
	cat $x >> $log
    fi
}

procs=`cat /proc/cpuinfo | grep "processor" | sort | tail -1 | cut -d ':' -f2 | sed -e 's/ //g'`
procs=`echo "$procs + 1" | bc`
