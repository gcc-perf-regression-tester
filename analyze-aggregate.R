#!/usr/bin/Rscript
#
# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

# This script is used to detect performance regressions in the time
# series of performance reports.  The commit time is used to sort the
# experiments.  When several experiments are available for a given
# commit, they are added at adjacent times in the time series.

options(warn = TRUE)

initial.options <- commandArgs(trailingOnly = FALSE)
file.arg.name <- "--file="
script.name <- sub(file.arg.name, "", initial.options[grep(file.arg.name, initial.options)])
script.path <- dirname(script.name)
source(paste(sep="/", script.path, "analyze-core.R"))

args <- commandArgs(TRUE)
x <- as.matrix (read.table ("normalized-aggregated.txt"))

for (column in 2:dim (x)[2]) {
  bmkName <- args[column - 1]

  t1 <- as.numeric (as.vector (x[,1]))
  x1 <- as.numeric (as.vector (x[,column]))

  notna <- !(is.na(t1) | is.na(x1))
  t2 <- t1[notna]
  x2 <- x1[notna]

  n <- noise (x2)
  t3 <- t2[!(n$indexes)]
  x3 <- x2[!(n$indexes)]
  averageStddev <- mean (n$stddevs)
  threshold <- 2.2 * averageStddev
  
  if (length (x3) > windowSize) {
    globalMin <- analyzeSlowdown (t3, x3, bmkName, threshold, averageStddev)
    doPlot (t3, x3, bmkName, globalMin, averageStddev)
  }
}

warnings ()
