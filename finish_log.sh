#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

# This file is sourced at the end of every benchmark in the go.sh
# script.  It completes the report with the information about the
# machine and then tags the report to the current revision that is
# checked out in the GCC git repo.

catlog /etc/hostname
catlog /etc/issue
catlog /proc/version
catlog /proc/meminfo
catlog /proc/cpuinfo

cd $X/gcc
git tag $tag -F $log

email=$2
if [ "x$email" != x ]; then
    cd $X/gcc
    git show $tag > $log.tag
    subject="Test results for $bmk with $config."
    mutt $email -s "$subject" -i $log.tag </dev/null
fi
