#!/bin/bash

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

# This script calls all the get.sh scripts in all the benchmark
# directories.  The get.sh scripts are launched in background to be
# processed in parallel.

X=$HOME/gcc/test
for d in `ls -d $X/bmks/*`; do
    if test -x $d/get.sh; then
        cd $d; ./get.sh &
    fi
done

