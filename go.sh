#!/bin/bash -x

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

# You can invoke this script like this:
#   echo "1fda60c695b7a1bb7303f79a0b6d4b792db0efe0" | ./go.sh
# This will build GCC and start the scripts in the bmks directory with
# the default configuration.
#
# Invoke this script as a gearman worker like this:
#   nohup gearman -h 10.236.48.91 -w -f tester ~/gcc/test/go.sh &
# then you can distribute work using a todo file like this:
#   gearman -b -h 10.236.48.91 -n -f tester < todo
#
# the format of each line in the todo file is "key config email" with
# key a valid SHA1 object, config the symbolic name for the compiler
# options to be used.  The minimal valid line contains only the SHA1
# key, the default config is ofast, and no email is sent.  For example
# the following would be a valid line in the todo file:
#   820c090b60266f0102253822c3d108e05f135014 ofast foo@bar.com
#
# To retrieve a list of SHA1 for all the patches containing "config/i386"
# in the last 500 commits, use this command:
#   git log --pretty=format:%H --grep="config/i386" HEAD~500..HEAD
#
# To fetch the tags from the test machines, one can use
#   git fetch --tags testmachine

X=$HOME/gcc/test
usr=
key=
config=
procs=`cat /proc/cpuinfo | grep "processor" | sort | tail -1 | cut -d ':' -f2 | sed -e 's/ //g'`
procs=`echo "$procs + 1" | bc`
ulimit -t 10000
mkdir -p $X/log/
mkdir -p $X/usr/

if ! test -d $X/gcc/.git/; then
    echo "Error: there is no GCC git repo in $X/gcc/."
    exit 1
fi

cd $X; git pull
cd $X/gcc; git pull

build_gcc () {
    usr=$X/usr/$key
    if test -x $usr/bin/gcc; then
	return 0
    fi

    cd $X/gcc
    if ! git checkout $key; then
	echo "Error: $key does not seem to be a valid SHA1 object in git repo $X/gcc/."
	return 1
    fi

    cd $X/gcc
    rm -rf $X/gcc/build-test $usr
    mkdir -p $X/gcc/build-test $usr
    cd $X/gcc/build-test
    ../configure --with-cloog --with-ppl --enable-cloog-backend=isl --prefix=$usr --disable-bootstrap --enable-languages=c,c++,fortran
    if ! make -j$procs ; then
	echo "Error: build of GCC failed."
	return 1
    fi
    if ! make install ; then
	echo "Error: install of GCC failed."
	return 1
    fi
}

read key config email
if ! build_gcc; then
    exit 0
fi

export LD_LIBRARY_PATH=$usr/lib64:$usr/lib:$LD_LIBRARY_PATH
export PATH=$usr/bin:$PATH
export CC=gcc
export CXX=g++
export FC=gfortran
export CPP=cpp
for d in `ls -d $X/bmks/*`; do
    if test -x $d/go.sh; then
	$d/go.sh $config $email
    fi
done
