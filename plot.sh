#!/bin/bash

# Copyright (C) 2011 Sebastian Pop <sebpop@gmail.com>.
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.  A copy of this license
# can be downloaded from http://www.gnu.org/copyleft/gpl.html

# This script is sourced at the end of every get.sh script.
#
# First, it gets the data from the tags of the benchmark BMK.  The
# function PARSE_NB is used to extract the interesting bits from the
# log files: this function is specific to each benchmark and thus it
# is defined in each get.sh script.
#
# Then, it calls gnuplot on the extracted data, and generates an
# index.html page.

X=$HOME/gcc/test
B=$X/bmks/$bmk
D=$B/data
mkdir -p $D/tags
mkdir -p $D/plot
mkdir -p $D/aggregated

cd $X/gcc/
for t in `git tag -l ${bmk}_*`; do
    tag="$D/tags/$t"
    if test -e $tag; then
	continue
    fi
    git show $t > $tag
    config=`grep -m1 "config file:" $tag | cut -d ':' -f2 | sed -e 's/ //g'`
    if test "x$config" = x; then
	config="ofast"
    fi
    model=`grep -m1 "model name" $tag | cut -d ':' -f2 | sed -e 's/^ //g' | sed -e 's/ /_/g'`
    mhz=`grep -m1 "cpu MHz" $tag | awk '{print $4}'`
    M="$D/plot/$model/$mhz/$config"
    mkdir -p "$M"

    out=""
    for b in $bmks; do
	nb=""
	parse_nb
	case "$nb" in
	    [1-9][0-9][0-9][0-9][0-9.]*)
		out="$out --";;
	    [1-9][0-9.]*)
		out="$out $nb";;
	    *)
		out="$out --";;
	esac
    done
    when=`grep "Date:" $tag | tail -1 | awk '{ $1 = ""; $NF = ""; print $0 }'`
    full=`date --date="$when" +%Y_%m_%d_%H_%M_%S`
    epoch=`date --date="$when" +%s`
    echo "$full $t $out" >> "$M/date.txt"
    echo "$epoch $out" >> "$M/epoch.txt"
done

get_analysis () {
    regression=`grep "Regression of" analysis.txt | grep "$b"`
    if test -n "$regression"; then
	overall=`echo $regression | awk '{$8 = strftime("%Y_%m_%d_%H_%M_%S", $8); print $0}'`
	cdir=`pwd | sed -e 's/(/\\\\\\\(/g' -e 's/)/\\\\\\\)/g'`
	grep "Slowdown of" analysis.txt | grep "$b" | sort | tail -20 | awk "{file=\$10 \"-\" \$12 \".txt\"; print \"cd $X/gcc; git log --since=\" \$10 \" --until=\" \$12 \" > $cdir/log-\" file \"; git log --pretty=format:%H --since=\" \$10 \" --until=\" \$12 \" > $cdir/sha-\" file | \"/bin/bash\"; \$10 = strftime(\"%Y_%m_%d_%H_%M_%S\", \$10); \$12 = strftime(\"%Y_%m_%d_%H_%M_%S\", \$12); print \$0 \"(<a href=\\\"log-\" file \"\\\">log</a>, <a href=\\\"sha-\" file \"\\\">sha</a>)<br>\"}" > details.txt
	details=`cat details.txt`
    fi
}

ubmks=`echo $bmks | sed -e 's|/|_|g'`

for model in `ls -1 "$D/plot"`; do
    for mhz in `ls -1 "$D/plot/$model"`; do
	for config in `ls -1 "$D/plot/$model/$mhz"`; do
	    cd "$D/plot/$model/$mhz/$config"
	    sort "date.txt" > "sorted.txt"
	    sort "epoch.txt" > "epoch-sorted.txt"

	    i=2
	    for b in $ubmks; do
		i=$(($i + 1))
		x="{ print \"(\" \$$i \")+\" }"
		sum=`awk "$x" sorted.txt`
		n_invalid=`echo "$sum" | grep -c "\-\-"`
		n=`wc -l sorted.txt | awk '{ print $1}'`
		if test $n -eq $n_invalid; then
		    continue
		fi
		valid_sum=`echo "$sum" | grep -v "\-\-"`
		sum=`echo "$valid_sum" | tr -d '\n'`
		mean=`echo "scale=10; ($sum 0) / ($n - $n_invalid)" | bc`
		sum=`echo "$valid_sum" | sed -e "s/)+/-$mean)^2 + /g" | tr -d '\n'`
		stddev=`echo "scale=10; sqrt (($sum 0) / ($n - $n_invalid))" | bc`
		nstddev=`echo "scale=10; ($stddev / $mean * 100)" | bc`
		gnuplot -e "set terminal png; set xdata time; set timefmt '%Y_%m_%d_%H_%M_%S'; set format x '%b'; plot $mean - $stddev with filledcurves y1=$mean linetype 1 linecolor rgb \"#bbddbb\" notitle, $mean + $stddev with filledcurves y1=$mean linetype 1 linecolor rgb \"#ddbbbb\" notitle, 'sorted.txt' using 1:$i title \"$b\" " > $b-time.png
		gnuplot -e "set terminal png; set xdata time; set timefmt '%Y_%m_%d_%H_%M_%S'; set format x '%b'; plot -$nstddev with filledcurves y1=0 linetype 1 linecolor rgb \"#bbddbb\" notitle, $nstddev with filledcurves y1=0 linetype 1 linecolor rgb \"#ddbbbb\" notitle, 'sorted.txt' using 1:(((\$$i - $mean) / $mean) * 100) title \"$b\" " > $b-normal.png
	    done

	    if test -x /usr/bin/Rscript; then
		$X/analyze.R $ubmks > "analysis.txt"
	    fi

	    id="$D/plot/$model/$mhz/$config/index.html"
	    title=""
	    title="$title `echo $model | cut -b 1-7`"
	    title="$title `echo $mhz | cut -b 1-4`"
	    title="$title `echo $config | cut -b 1-7`"
	    echo "<html><head><title>$title</title></head><table>" > $id
	    echo "<tr><th>Benchmark</th><th>Execution time in seconds (lower is better)</th><th>Normalized percentage: (x - mean) / mean * 100 <br> and last global minimum</th><th>Analysis results</th></tr>" >> $id
	    echo "<tr><td>Overall</td><td><a href=\"all-time.png\"><img src=\"all-time.png\"/></a></td><td><a href=\"all-normal.png\"><img src=\"all-normal.png\"/></a></td></tr>" >> $id
	    for b in $ubmks; do
		overall=""
		details=""
		get_analysis
		echo "<tr><td>$b</td><td><a href=\"$b.html\"><img src=\"$b-time.png\"/></a></td><td><a href=\"$b.html\"><img src=\"$b-R.png\"/></a></td><td> <span style=\"color:red\"> $overall </span> <br> $details </td></tr>" >> $id
	    done
	    echo "</table>" >> $id
	    echo "<span style=\"color:red\">" >> $id
	    grep "Regression of" analysis.txt | awk '{ $6 = "<a href="$6".html>"$6"</a>"; $8 = strftime("%Y_%m_%d_%H_%M_%S", $8); print $0 "<br>"}' | sort -r >> $id
	    echo "</span>" >> $id
	    echo "<a href=\"numbers.html\">Detailed numbers</a>" >> $id
	    echo "</html>" >> $id

	    id="$D/plot/$model/$mhz/$config/numbers.html"
	    echo "<html><head><title>$title</title></head><table>" > $id
	    echo "<table><tr>" >> $id
	    echo "<th>Commit time</th>" >> $id
	    echo "<th>Tag</th>" >> $id
	    for b in $ubmks; do
		echo "<th><a href=\"$b.html\">$b</a></th>" >> $id
	    done
	    echo "</tr>" >> $id
	    awk '{ print "<tr><td>" $1 "</td><td><a href=\"../../../../tags/" $2 "\">" $2 "</a></td>"; for (i=3; i<=NF; i++) print "<td>" $i "</td>"; print "</tr>" }' sorted.txt >> $id
	    echo "</table></html>" >> $id

	    i=2
	    for b in $ubmks; do
		i=$(($i + 1))
		id="$D/plot/$model/$mhz/$config/$b.html"
		overall=""
		details=""
		get_analysis
		echo "<html><head><title>$b</title></head><table>" > $id
		echo "<tr><th>Execution time in seconds (lower is better)</th><th>Normalized percentage: (x - mean) / mean * 100 <br> and last global minimum</th><th>Analysis results</th></tr>" >> $id
		echo "<tr><td><a href=\"$b-time.png\"><img src=\"$b-time.png\"/></a></td><td><a href=\"$b-R.png\"><img src=\"$b-R.png\"/></a></td><td> <span style=\"color:red\"> $overall </span> <br> $details </td></tr></table>" >> $id

		echo "<table><tr><th>Commit time</th><th>Tag</th><th>$b</th></tr>" >> $id
		awk "{ print \"<tr><td>\" \$1 \"</td><td><a href=\\\"../../../../tags/\" \$2 \"\\\">\" \$2 \"</a></td><td>\" \$$i \"</td></tr>\" }" sorted.txt >> $id
		echo "</table></html>" >> $id
	    done
	done
    done
done

cd $D/aggregated/
i=2
cx="set terminal png; set key below; set xdata time; set timefmt '%Y_%m_%d_%H_%M_%S'; set format x '%b'; plot"
cta="$cx"
cna="$cx"
for b in $ubmks; do
    i=$(($i + 1))
    ct="$cx"
    cn="$cx"
    for model in `ls -1 "$D/plot"`; do
	for mhz in `ls -1 "$D/plot/$model"`; do
	    for config in `ls -1 "$D/plot/$model/$mhz"`; do
		sorted="$D/plot/$model/$mhz/$config/sorted.txt"
		n=`wc -l $sorted | awk '{ print $1}'`
		if test $n -ge 10; then
		    title=""
		    title="$title `echo $model | cut -b 1-7`"
		    title="$title `echo $mhz | cut -b 1-4`"
		    title="$title `echo $config | cut -b 1-7`"
		    ct="$ct '$sorted' using 1:$i title \"$title\","
		    cta="$cta '$sorted' using 1:$i notitle,"

		    x="{ print \"(\" \$$i \")+\" }"
		    sum=`awk "$x" $sorted`
		    n_invalid=`echo "$sum" | grep -c "\-\-"`
		    if test $n -eq $n_invalid; then
			continue
		    fi

		    valid_sum=`echo "$sum" | grep -v "\-\-"`
		    sum=`echo "$valid_sum" | tr -d '\n'`
		    mean=`echo "scale=10; ($sum 0) / ($n - $n_invalid)" | bc`
		    sum=`echo "$valid_sum" | sed -e "s/)+/-$mean)^2 + /g" | tr -d '\n'`
		    stddev=`echo "scale=10; sqrt (($sum 0) / ($n - $n_invalid))" | bc`
		    cn="$cn '$sorted' using 1:((abs(\$$i - $mean) / 3 < $stddev ) ? ((\$$i - $mean) / $mean) * 100 : 1/0) title \"$title\","
		    cna="$cna '$sorted' using 1:((abs(\$$i - $mean) / 3 < $stddev ) ? ((\$$i - $mean) / $mean) * 100 : 1/0) notitle,"
		fi
	    done
	done
    done
    c=`echo $ct | sed -e 's/,$//g'`
    gnuplot -e "$c" > $b-time.png
    gnuplot -e "$cn 0 with lines notitle" > $b-normal.png
done
c=`echo $cta | sed -e 's/,$//g'`
gnuplot -e "$c" > all-time.png
gnuplot -e "$cna 0 with lines notitle" > all-normal.png


cd $D/aggregated/
rm $D/aggregated/*-normalized.txt
for model in `ls -1 "$D/plot"`; do
    for mhz in `ls -1 "$D/plot/$model"`; do
	for config in `ls -1 "$D/plot/$model/$mhz"`; do
	    sorted="$D/plot/$model/$mhz/$config/epoch-sorted.txt"
	    normalized="$D/aggregated/$model-$mhz-$config-normalized.txt"
	    n=`wc -l $sorted | awk '{ print $1}'`
	    if test $n -ge 10; then
		cp $sorted $normalized
		i=1
		for b in $ubmks; do
		    i=$(($i + 1))
		    x="{ print \$$i \"+\" }"
		    sum=`awk "$x" $sorted`
		    n_invalid=`echo "$sum" | grep -c "\-\-"`
		    if test $n -eq $n_invalid; then
			continue
		    fi

		    valid_sum=`echo "$sum" | grep -v "\-\-"`
		    sum=`echo "$valid_sum" | tr -d '\n'`
		    mean=`echo "scale=10; ($sum 0) / ($n - $n_invalid)" | bc`
		    x="{ y = ((\$$i - $mean) / $mean * 100); \$$i = (y == -100) ? \"--\" : y; print \$0 }"
		    awk "$x" $normalized > tmp
		    mv tmp $normalized
		done
	    fi
	done
    done
done

aggregated="$D/aggregated/normalized-aggregated.txt"
> $aggregated
for f in `ls *-normalized.txt`; do
    cat $f >> $aggregated
done

sort $aggregated > tmp
mv tmp $aggregated

if test -x /usr/bin/Rscript; then
    $X/analyze-aggregate.R $ubmks > "analysis.txt"
fi

id="$D/aggregated/index.html"
echo "<html><head><title>$bmk aggregated graphs</title></head><table>" > $id
echo "<tr><th>Benchmark</th><th>Execution time in seconds (lower is better)</th><th>Normalized percentage: (x - mean) / mean * 100 <br> and last global minimum</th><th>Analysis results</th></tr>" >> $id
echo "<tr><td>Overall</td><td><a href=\"all-time.png\"><img src=\"all-time.png\"/></a></td><td><a href=\"all-normal.png\"><img src=\"all-normal.png\"/></a></td></tr>" >> $id
for b in $ubmks; do
    overall=""
    details=""
    get_analysis
    echo "<tr><td>$b</td><td><a href=\"$b.html\"><img src=\"$b-time.png\"/></a></td><td><a href=\"$b.html\"><img src=\"$b-R.png\"/></a></td><td> <span style=\"color:red\"> $overall </span> <br> $details </td></tr>" >> $id
done
echo "</table>" >> $id
echo "<span style=\"color:red\">" >> $id
grep "Regression of" analysis.txt | awk '{ $6 = "<a href="$6".html>"$6"</a>"; $8 = strftime("%Y_%m_%d_%H_%M_%S", $8); print $0 "<br>"}' | sort -r >> $id
echo "</span>" >> $id
echo "<a href=\"numbers.html\">Detailed numbers</a>" >> $id
echo "</html>" >> $id

id="$D/aggregated/numbers.html"
echo "<html><head><title>$bmk aggregated numbers</title></head><table>" > $id
echo "<table><tr>" >> $id
echo "<th>Model/MHz</th>" >> $id
echo "<th>Commit time</th>" >> $id
echo "<th>Tag</th>" >> $id
for b in $ubmks; do
    echo "<th><a href=\"$b.html\">$b</a></th>" >> $id
done
echo "</tr>" >> $id
for model in `ls -1 "$D/plot"`; do
    for mhz in `ls -1 "$D/plot/$model"`; do
	for config in `ls -1 "$D/plot/$model/$mhz"`; do
	    if test `wc -l $D/plot/$model/$mhz/$config/sorted.txt | awk '{ print $1}'` -ge 10; then
		echo "<tr><td><a href =\"../plot/$model/$mhz/$config/index.html\">${model}_${mhz}_${config}</a></td></tr>" >> $id
	    fi
	done
    done
done
for model in `ls -1 "$D/plot"`; do
    for mhz in `ls -1 "$D/plot/$model"`; do
	for config in `ls -1 "$D/plot/$model/$mhz"`; do
	    if test `wc -l $D/plot/$model/$mhz/$config/sorted.txt | awk '{ print $1}'` -ge 10; then
		echo "<tr><td><a href =\"../plot/$model/$mhz/$config/index.html\">${model}_${mhz}_${config}</a></td></tr>" >> $id
		awk '{ print "<tr><td></td><td>" $1 "</td><td><a href=\"../tags/" $2 "\">" $2 "</a></td>"; for (i=3; i<=NF; i++) print "<td>" $i "</td>"; print "</tr>" }' $D/plot/$model/$mhz/$config/sorted.txt >> $id
	    fi
	done
    done
done
echo "</table></html>" >> $id

i=2
for b in $ubmks; do
    i=$(($i + 1))
    id="$D/aggregated/$b.html"
    overall=""
    details=""
    get_analysis
    echo "<html><head><title>$b aggregated</title></head><table>" > $id
    echo "<tr><th>Execution time in seconds (lower is better)</th><th>Normalized percentage: (x - mean) / mean * 100 <br> and last global minimum</th><th>Analysis results</th></tr>" >> $id
    echo "<tr><td><a href=\"$b-time.png\"><img src=\"$b-time.png\"/></a></td><td><a href=\"$b-R.png\"><img src=\"$b-R.png\"/></a></td><td> <span style=\"color:red\"> $overall </span> <br> $details </td></tr></table>" >> $id
    echo "</table><table><tr><th>Model/MHz</th><th>Commit time</th><th>Tag</th><th>$b</th></tr>" >> $id
    for model in `ls -1 "$D/plot"`; do
	for mhz in `ls -1 "$D/plot/$model"`; do
	    for config in `ls -1 "$D/plot/$model/$mhz"`; do
		if test `wc -l $D/plot/$model/$mhz/$config/sorted.txt | awk '{ print $1}'` -ge 10; then
		    echo "<tr><td><a href =\"../plot/$model/$mhz/$config/$b.html\">${model}_${mhz}_${config}</a></td></tr>" >> $id
		fi
	    done
	done
    done
    for model in `ls -1 "$D/plot"`; do
	for mhz in `ls -1 "$D/plot/$model"`; do
	    for config in `ls -1 "$D/plot/$model/$mhz"`; do
		if test `wc -l $D/plot/$model/$mhz/$config/sorted.txt | awk '{ print $1}'` -ge 10; then
		    echo "<tr><td><a href =\"../plot/$model/$mhz/$config/$b.html\">${model}_${mhz}_${config}</a></td></tr>" >> $id
		    awk "{ print \"<tr><td></td><td>\" \$1 \"</td><td><a href=\\\"../tags/\" \$2 \"\\\">\" \$2 \"</a></td><td>\" \$$i \"</td></tr>\" }" $D/plot/$model/$mhz/$config/sorted.txt >> $id
		fi
	    done
	done
    done
    echo "</table></html>" >> $id
done
